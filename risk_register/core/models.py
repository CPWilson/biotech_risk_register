from django.db import models

# Create your models here.

class ControlledVocabulary(models.Model):
	category = models.CharField(max_length=255)
	value = models.CharField(max_length=255)

class Project(models.Model):
	name = models.CharField(max_length=255, unique=True)
	verbose_name = models.CharField(max_length=255, blank=True)
	active = models.BooleanField(default=True)
	
	def __repr__(self):
		return self.id
		
class Risk(models.Model):
	pass