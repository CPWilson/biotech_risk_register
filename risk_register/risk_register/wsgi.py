"""
WSGI config for risk_register project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

import sys

root_path = os.path.abspath(os.path.split(os.path.split(__file__)[0])[0])
sys.path.insert(0, root_path)

application = get_wsgi_application()
